import { SET_EMAIL_ACTION } from "../constants/setEmailAction";

const initialState = {
    email: '',
}
export const emailReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case SET_EMAIL_ACTION: return {...state, email: payload}

        default: return state
    }
}
