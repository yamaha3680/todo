import { SET_EMAIL_ACTION } from "../constants/setEmailAction";

export const setEmail = (value) => {
    return {
        type: SET_EMAIL_ACTION,
        payload: value
    }
}
