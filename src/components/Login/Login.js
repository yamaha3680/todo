import './Login.css'
import Offer from '../Offer/Offer'
import Button from "../Button/Button";
import {useHistory} from 'react-router-dom'
import {useStore} from "react-redux";
import {setEmail} from "../../store/actionCreators/setEmail";


const Login = () => {
    const his = useHistory()
    const store = useStore()

    const reg = () => {
        his.push('register')
    }

    const log = () => {
        store.dispatch(setEmail(document.getElementById('login').value))
    }

    return (
        <div className="container">
            <div className="row">
                <Offer/>

                <div
                    className='col-lg-6'
                    id='loginPanel'
                >
                    <form method="post">
                        <div className='logForm'>
                            <label>Логин </label>
                            <input
                                id='login'
                                name='login'
                                type="text"
                                placeholder='введите логин'
                            />
                        </div>
                        <div className='logForm'>
                            <label>Пароль </label>
                            <input
                                id='password'
                                name='password'
                                type="password"
                                placeholder='введите пароль'
                            />
                        </div>
                    </form>

                    <div className='d-flex'>
                        <Button
                            name="Регистрация"
                            callback={reg}
                        />
                        <Button
                            name="Вход"
                            callback={log}
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login