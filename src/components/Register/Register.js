import './Register.css'
import Offer from "../Offer/Offer";
import Button from "../Button/Button";
import {useHistory} from "react-router-dom";
import {useStore} from "react-redux";
import {setEmail} from "../../store/actionCreators/setEmail";

const Register = () => {
    const his = useHistory()
    const store = useStore()

    const log = () => {
        his.push('login')
    }

    const reg = () => {
        store.dispatch(setEmail(document.getElementById('register').value))
    }

    return (
        <div className="container">
            <div className="row">
                <Offer/>

                <div
                    className='col-lg-6'
                    id='loginPanel'
                >
                    <form method="post">
                        <div className='form'>
                            <label>Логин </label>
                            <input
                                id='register'
                                name='register'
                                type="text"
                                placeholder='введите логин'
                            />
                        </div>
                        <div className='form'>
                            <label>Повторите логин </label>
                            <input
                                id='reRegister'
                                name='reRegister'
                                type="text"
                                placeholder='повторите логин'
                            />
                        </div>
                        <div className='form'>
                            <label>Пароль </label>
                            <input
                                id='password'
                                name='password'
                                type="password"
                                placeholder='введите пароль'
                            />
                        </div>
                    </form>

                    <div className='d-flex'>
                        <Button
                            name="Вход"
                            callback={log}
                        />
                        <Button
                            name="Регистрация"
                            callback={reg}
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Register