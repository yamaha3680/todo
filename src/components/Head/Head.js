import './Head.css'
import 'bootstrap/dist/css/bootstrap-grid.min.css'
import logo from '../../imgs/logo.png'
import {useState} from 'react'
import {useStore} from "react-redux";

const Head = () => {
    const [email, setEmail] = useState('')

    const store = useStore()

    store.subscribe(() => {
        const storeEmail = store.getState().emailReduser.email
            setEmail(storeEmail)
    })

    return (
        <header className="Head navbar-fixed-top">
            <div className="headContainer container d-flex">
                <div className="row align-self-center container-fluid">
                    <div className="container">
                        <div className="logoContainer row">
                            <div className="container col-3">
                                <div className="row">
                                    <img
                                        id="imgLogo"
                                        className="col-3"
                                        src={logo}
                                        alt=""
                                    />
                                    <div className="logoTxt col-auto align-self-center">ToDoshnick</div>
                                </div>
                            </div>

                            <div className="userEmail col-4 offset-5 align-self-center">
                                <div className="emailBack">
                                    <div className="emailFront">
                                        <div id="emailTxt">
                                            {
                                                (email === '') && "войдите в систему"
                                            }
                                            {
                                                !(email === '') && email
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Head