import './Button.css'
import PropTypes from 'prop-types'

// const left = {position}

const Button = (props) => {


    return (
        <button className='button' onClick={props.callback}>
            {props.name}
        </button>
    )
}

Button.propTypes = {
    name: PropTypes.string,
    callback: PropTypes.func,
    left: PropTypes.bool
}

export default Button