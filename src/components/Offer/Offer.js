import './Offer.css'

const Offer = () => {
    return (
        <div
            id='offer'
            className='col-lg-6'
        >
            <div id="offerBigTxt">Сделай свой ToDo удобней</div>
            <div id="offerTxt">Присоединяйся и выполняй свои ежедневные ToDo с нами</div>
        </div>
    )
}

export default Offer