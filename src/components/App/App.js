import './App.css';
import Head from '../Head/Head'
import Workspace from "../Workspace/Workspace";

const App = () => {
    return (
        <div className='App'>
            <Head/>
            <Workspace/>
        </div>
    )
}

export default App;