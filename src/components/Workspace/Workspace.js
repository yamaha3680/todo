import './Workspace.css'
import React from "react";
import Login from "../Login/Login";
import Todos from "../Todos/Todos";
import Register from "../Register/Register";
import {Switch, Route, useHistory} from 'react-router-dom'

// login todos register
const Workspace = () => {
    const his = useHistory()

    return <div className="Workspace">
        <Switch>
            <Route exact path='/'>
                {
                    his.push('login')
                }
            </Route>
            <Route path='/login' component={Login}/>
            <Route path='/register' component={Register}/>
            <Route path='/todo' component={Todos}/>
        </Switch>
    </div>

}

export default Workspace